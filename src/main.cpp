#include <iostream>
#include <memory>
#include <thread>
#include "grintEngine.h"

using namespace std;

class DefaultController : public Controller {
public:
    DefaultController() : Controller(false) {}
    bool key_press(RenderWindow* render, int key, int scancode, int action, int mods) override {
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
            render->close();
            return true;
        } else if (key == GLFW_KEY_ENTER && action == GLFW_PRESS && mods == GLFW_MOD_ALT) {
            if (glfwGetWindowAttrib(render->getGLFWwindow(), GLFW_MAXIMIZED) != 0)
                glfwRestoreWindow(render->getGLFWwindow());
            else
                glfwMaximizeWindow(render->getGLFWwindow());
            return true;
        }
        return false;
    }
    bool mouse_button(RenderWindow *render, int button, int action, int mods) override { return false; }
    bool mouse_move(RenderWindow *render, double x, double y) override { return false; }
    bool mouse_wheel(RenderWindow *render, double xoffset, double yoffset) override { return false; }
    bool key_poll(RenderWindow *render, GLFWwindow *glwindow) override { return false; }
};

//render test
size_t mat_red;
VertexGroup group;

void TestScene(RenderWindow* render, double dt) {
    // main render
    group.draw();
    group.rotation[0]+=dt*36;

    render->_startLineRender();

    // line render

    render->_endLineRender();
}

int main(int argc, char** argv) {
    //test stuff
    mat_red = MaterialAtlas.add("mat_red", Material().setColor({1,0,0}));
    group = { //struct initialization
            .vertices={{
                               .position={0.7,0,-0.7}
                       },{
                               .position={-0.7,0,-0.7}
                       },{
                               .position={0,0,1}
                       }},
            .polygons={{
                               .p={2,1,0},
                               .m=mat_red
                       }},
            .offset={0,1,0},
            .rotation={0,0,0}
    };

    RenderWindow* game;
    try {
        game = new RenderWindow(Material().setColor(TaintTexture(1.0f,0.0f,1.0f)));
        game->pushKeybinds(DefaultController());
        game->setSceneRender(TestScene);
        this_thread::sleep_for(1s);
    } catch (exception& e) {
        cerr << e.what() << endl;
        return -1;
    }

    startGameLoop(game);

    return 0;
}