#include "GlfwInstances.h"
#include <exception>
#include <algorithm>

//static in .h makes this a forward declaration. definition in .c required
map<GLFWwindow*, RenderWindow*> GLFW_CB::instanceMap;

void GLFW_CB::bind(GLFWwindow* glwindow, RenderWindow* instance) {
    auto it = instanceMap.find(glwindow);
    if (it != instanceMap.end()) {
        throw runtime_error("GLFW instance already bound");
    }
    auto it2 = find_if(instanceMap.begin(), instanceMap.end(), [instance](const auto& element){return element.second == instance;});
    if (it != instanceMap.end()) {
        throw runtime_error("RenderWindow instance already bound");
    }
    glfwSetErrorCallback(gl_static_callback_error);
    glfwSetFramebufferSizeCallback(glwindow, gl_global_callback_framebuffer_size);
    glfwSetCursorPosCallback(glwindow, gl_global_callback_cursor_position);
    glfwSetMouseButtonCallback(glwindow, gl_global_callback_mouse_button);
    glfwSetScrollCallback(glwindow, gl_global_callback_scroll_callback);
    glfwSetKeyCallback(glwindow, gl_global_callback_keystroke);
    glfwSetWindowCloseCallback(glwindow, gl_global_callback_close);
    glfwSetWindowMaximizeCallback(glwindow, gl_static_callback_windowmode);
    instanceMap.emplace(glwindow, instance);
}

void GLFW_CB::release(GLFWwindow* glwindow) {
    auto it = instanceMap.find(glwindow);
    if (it == instanceMap.end()) {
        throw runtime_error("The specified glwindow was never bound to a render instance (release)");
    }
    glfwSetFramebufferSizeCallback(glwindow, nullptr);
    glfwSetCursorPosCallback(glwindow, nullptr);
    glfwSetMouseButtonCallback(glwindow, nullptr);
    glfwSetScrollCallback(glwindow, nullptr);
    glfwSetKeyCallback(glwindow, nullptr);
    glfwSetWindowCloseCallback(glwindow, nullptr);
    glfwDestroyWindow(glwindow);
    instanceMap.erase(it);
}
RenderWindow* GLFW_CB::find(GLFWwindow* glwindow) {
    auto it = instanceMap.find(glwindow);
    if (it == instanceMap.end()) {
        throw runtime_error("The specified glwindow was never bound to a render instance (release)");
    }
    return it->second;
}

void gl_global_callback_framebuffer_size(GLFWwindow *window, int width, int height) {
    GLFW_CB::find(window)->gl_framebuffer_size_callback(width, height);
}

void gl_global_callback_cursor_position(GLFWwindow *window, double xpos, double ypos) {
    GLFW_CB::find(window)->gl_cursor_position_callback(xpos, ypos);
}

void gl_global_callback_mouse_button(GLFWwindow *window, int button, int action, int mods) {
    GLFW_CB::find(window)->gl_mouse_button_callback(button, action, mods);
}

void gl_global_callback_scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {
    GLFW_CB::find(window)->gl_scroll_callback(xoffset, yoffset);
}

void gl_global_callback_keystroke(GLFWwindow *window, int key, int scancode, int action, int mods) {
    GLFW_CB::find(window)->gl_key_callback(key, scancode, action, mods);
}

void gl_global_callback_close(GLFWwindow *window) {
    GLFW_CB::find(window)->close();
}

void gl_static_callback_windowmode(GLFWwindow *window, int maximized) {
    int sx,sy,sw,sh;
    if (maximized) {
        GLFWmonitor* monitor = glfwGetPrimaryMonitor();
        glfwGetMonitorWorkarea(monitor,&sx,&sy,&sw,&sh);
        glfwSetWindowMonitor(window, monitor, sx, sy, sw, sh, GLFW_DONT_CARE);
    } else {
        glfwGetWindowPos(window, &sx, &sy);
        glfwGetWindowSize(window, &sw, &sh);
        glfwSetWindowMonitor(window, nullptr, sx, sy, sw, sh, GLFW_DONT_CARE);
    }
}

void gl_static_callback_error(int error, const char *description) {
    std::cerr << "GLFE Error " << error << std::endl << description << std::endl;
}
