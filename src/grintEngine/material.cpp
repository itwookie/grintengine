#include "material.h"
#include <png.h>
#include <zlib.h>
#include <algorithm>

void ImageTexture::constructRaw(const uint8_t *memory, size_t width, size_t height) {
    glGenTextures(1, &hdlTexture);
    glBindTexture(GL_TEXTURE_2D, hdlTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, height, width, 0, GL_RGBA, GL_UNSIGNED_BYTE, memory);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    //glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);
}

ImageTexture::ImageTexture(const char *pngfilename) {
    try {
        png_image image;
        memset(&image, 0, sizeof(image));
        image.version = PNG_IMAGE_VERSION;
        if (png_image_begin_read_from_file(&image, pngfilename)) {
            png_bytep buffer;
            image.format = PNG_FORMAT_RGBA;
            size_t imageSize = PNG_IMAGE_SIZE(image);
            buffer = (png_bytep)malloc(imageSize);
            if (buffer != NULL &&
                png_image_finish_read(&image, NULL/*background*/, buffer, 0/*row_stride*/, NULL/*colormap*/) != 0)
            {
                constructRaw(buffer, image.width, image.height);

                free(buffer);
                png_image_free(&image);
            } else {
                string msg = string("Could not load texture ") + pngfilename;
                throw runtime_error(msg);
            }
        } else {
            png_image_free(&image);
            string msg = string("Unsupported Texture: ") + pngfilename;
            throw runtime_error(msg);
        }
    } catch (exception& e) {
        cerr << e.what() << endl;
    }
}

ImageTexture::ImageTexture(const uint8_t *memory, size_t offset, size_t width, size_t height) {
    constructRaw(memory+offset, width, height);
}
ImageTexture::ImageTexture(const uint8_t *memory, size_t offset, size_t size, size_t decompSize, size_t width, size_t height) {
    uint8_t* buffer;
    uLongf bufferSize = decompSize;
    buffer = (uint8_t*)malloc(bufferSize);
    int rc = uncompress(buffer, &bufferSize, memory+offset, size);
    if (rc == Z_MEM_ERROR) {
        throw runtime_error("Not enough memory to decompress ImageTexture");
    } else if (rc == Z_BUF_ERROR) {
        throw runtime_error("Decompression buffer not big enough for ImageTexture");
    } else if (rc == Z_DATA_ERROR || bufferSize != decompSize) {
        throw runtime_error("Compressed Date for ImageTexture incomplete or corrupted");
    } else if (rc != Z_OK) {
        throw runtime_error("Unexpected decompression error for ImageTexture");
    } else {
        constructRaw(buffer, width, height);
    }
    delete(buffer);
}

ImageTexture::~ImageTexture() {
    if (glIsTexture(hdlTexture)) glDeleteTextures(1, &hdlTexture);
}

void ImageTexture::apply() const {
    glBindTexture(GL_TEXTURE_2D, hdlTexture);
}

Material::Material():
        ambientTaint(0.2f),diffuseTaint(0.8f),specularTaint(0.0f),specularShininess(0),emissionTaint(0.0f),
        colorTaint(1.0f,0.0f,1.0f),colorTexture() {
}

void Material::bind() const {
    ambientTaint.apply(GL_AMBIENT);
    diffuseTaint.apply(GL_DIFFUSE);
    specularTaint.apply(GL_SPECULAR);
    glMaterialf(GL_FRONT, GL_SHININESS, specularShininess);
    emissionTaint.apply(GL_EMISSION);
    colorTaint.apply();
    if (colorTexture) colorTexture->apply(); else glBindTexture(GL_TEXTURE_2D, 0);
}

Material& Material::setColor(const TaintTexture& taint) { colorTaint = taint; return *this; }
Material& Material::setAmbient(const TaintTexture& taint) { ambientTaint = taint; return *this; }
Material& Material::setDiffuse(const TaintTexture& taint) { diffuseTaint = taint; return *this; }
Material& Material::setAmbientDiffuse(const TaintTexture& taint) { ambientTaint = diffuseTaint = taint; return *this; }
Material& Material::setSpecular(const TaintTexture& taint) { specularTaint = taint; return *this; }
Material& Material::setShininess(float shininess) { specularShininess = shininess; return *this; }
Material& Material::setEmission(const TaintTexture& taint) { emissionTaint = taint; return *this; }
Material& Material::setMaterial(const ImageTexture& texture) { colorTexture = texture; return *this; }

void Material::reset() {
    static TaintTexture ttBlack(0.0f);
    static TaintTexture ttDark(0.2f);
    static TaintTexture ttGray(0.8f);
    static TaintTexture ttColor(1.0f,0.0f,1.0f);
    ttDark.apply(GL_AMBIENT);
    ttGray.apply(GL_DIFFUSE);
    ttBlack.apply(GL_SPECULAR);
    glMaterialf(GL_FRONT, GL_SHININESS, 0);
    ttBlack.apply(GL_EMISSION);
    ttColor.apply();
    glBindTexture(GL_TEXTURE_2D, 0);
}

MaterialAtlas_t MaterialAtlas;

size_t MaterialAtlas_t::add(const string& name, Material& material) {
    //check if we know this material
    ssize_t nextHdl = getHandle(name);
    if (nextHdl >= 0) { //already registered
        usecount[hdlIdx[nextHdl]]++; // increase use count
        return nextHdl;
    }
    //get the next handle to return
    if (!freeHdls.empty()) nextHdl = *(freeHdls.erase(freeHdls.begin()));
    else nextHdl = maxHandle++;
    // create hdl-idx mapping (because rn materials.size() is the new materials index)
    hdlIdx[nextHdl]=materials.size();
    // initialize data
    materials.push_back(material);
    usecount.push_back(1);
    names.push_back(name);

    return nextHdl;
}

ssize_t MaterialAtlas_t::getHandle(const string& name) {
    auto it = find(names.begin(), names.end(), name);
    if (it == names.end()) return -1;
    size_t idx = it-names.begin();
    auto hdl = find_if(hdlIdx.begin(), hdlIdx.end(), [idx](const auto& elem){return elem.second == idx;});
    if (hdl == hdlIdx.end()) return -1; //should not happen but eh
    return hdl->first;
}

Material& MaterialAtlas_t::getMaterial(size_t handle) {
    return materials.at(hdlIdx.at(handle)); //at() is checked, i don't want key creation here
}

void MaterialAtlas_t::remove(size_t handle) {
    auto idx = hdlIdx.find(handle);
    if (idx == hdlIdx.end()) return; //already freed
    size_t index = idx->second;
    // remove usage
    size_t uses = usecount[index]--;
    if (uses == 0) {
        materials.erase(materials.begin()+index);
        usecount.erase(usecount.begin()+index);
        names.erase(names.begin()+index);
        //fix indices
        for_each(hdlIdx.begin(), hdlIdx.end(), [index](auto& entry){if (entry.second > index) entry.second--;});
        //mark this handle as free
        freeHdls.push_back(handle);
        //reduce size of freeHdls as much as possible
        for (auto it = find(freeHdls.begin(), freeHdls.end(), maxHandle-1); it != freeHdls.end(); it = find(freeHdls.begin(), freeHdls.end(), maxHandle-1)){
            freeHdls.erase(it);
            maxHandle--;
        }

    }
}
