#include "RenderWindow.h"
#include "GlfwInstances.h"
#include "vertexGroup.h"

void RenderWindow::gl_framebuffer_size_callback(int width, int height) {
    float ratio = width / (float) height;
    glViewport(0, 0, width, height);

    Perspective(ratio, 1e-1, 1e6);
    glMatrixMode(GL_MODELVIEW);
    LoadIdentity();
}

void RenderWindow::gl_cursor_position_callback(double xpos, double ypos) {
    if (keyBinds.empty()) return;
    for (auto it = keyBinds.rbegin(); it != keyBinds.rend(); it++) {
        if ((*it)->mouse_move(this, xpos, ypos) || !(*it)->isTransparent()) return;
    }
}

void RenderWindow::gl_mouse_button_callback(int button, int action, int mods) {
    if (keyBinds.empty()) return;
    for (auto it = keyBinds.rbegin(); it != keyBinds.rend(); it++) {
        if ((*it)->mouse_button(this, button, action, mods) || !(*it)->isTransparent()) return;
    }
}

void RenderWindow::gl_scroll_callback(double xoffset, double yoffset) {
    if (keyBinds.empty()) return;
    for (auto it = keyBinds.rbegin(); it != keyBinds.rend(); it++) {
        if ((*it)->mouse_wheel(this, xoffset, yoffset) || !(*it)->isTransparent()) return;
    }
}

void RenderWindow::gl_key_callback(int key, int scancode, int action, int mods) {
    if (keyBinds.empty()) return;
    for (auto it = keyBinds.rbegin(); it != keyBinds.rend(); it++) {
        if ((*it)->key_press(this, key, scancode, action, mods) || !(*it)->isTransparent()) return;
    }
}

void RenderWindow::processInput() {
    if (keyBinds.empty()) return;
    for (auto it = keyBinds.rbegin(); it != keyBinds.rend(); it++) {
        if ((*it)->key_poll(this, this->window) || !(*it)->isTransparent()) return;
    }
}

void RenderWindow::camera_lookAt(vec &c, vec &t) {
    arma::vec dir = t-c;
    dir = arma::normalise(dir);

    //creating yaw
    float gieren = atan2(dir[0], dir[1]);//-(_PI/2);
    //creating pitch
    float nicken = -asin(dir[2]);
    //create trans-rotation matrix
    glRotated(nicken*toDeg, 1.0, 0.0, 0.0);
    glRotated(gieren*toDeg, 0.0, 0.0, 1.0);
    glTranslated(-c[0], -c[1], -c[2]);
}

void RenderWindow::rotatePoint(const vec &center, vec &point, const double zaxis, const double xaxis) {
    arma::vec dir = point-center;
    arma::vec up = {0,0,1};
    arma::vec right = arma::normalise(arma::cross(dir, up));
    up = arma::normalise(arma::cross(right, dir));

    float cz = cos(zaxis), sz = sin(zaxis);
    float cx = cos(xaxis), sx = sin(xaxis);
    float ux = up[0], uy = up[1], uz = up[2];
    float rx = right[0], ry = right[1], rz = right[2];
    float mcz = 1-cz, mcx = 1-cx;
    arma::mat rotZ = {
            { cz+ux*ux*mcz, ux*uy*mcz-uz*sz, ux*uz*mcz+uy*sz },
            { ux*uy*mcz+uz*sz, cz+uy*uy*mcz, uy*uz*mcz-ux*sz },
            { ux*uz*mcz-uy*sz, uy*uz*mcz+ux*sz, cz+uz*uz*mcz }
    };
    arma::mat rotX = {
            { cx+rx*rx*mcx, rx*ry*mcx-rz*sx, rx*rz*mcx+ry*sx },
            { rx*ry*mcx+rz*sx, cx+ry*ry*mcx, ry*rz*mcx-rx*sx },
            { rx*rz*mcx-ry*sx, ry*rz*mcx+rx*sx, cx+rz*rz*mcx }
    };
    arma::vec newDir = rotZ*rotX*dir;

    point[0] = center[0]+newDir[0];
    point[1] = center[1]+newDir[1];
    point[2] = center[2]+newDir[2];
}

void RenderWindow::switchMode(bool to3d) {
    if (mode3d == to3d) return;
    mode3d = to3d;
    Perspective(aspectRatio, clipPlaneNear, clipPlaneFar);
    LoadIdentity();
}

void RenderWindow::LoadIdentity() {
    //write a rotation matrix
    //this will swap the back and up axis for the identity matrix
    //creating a right-hand system (x->right, z->up)
    glMatrixMode(GL_MODELVIEW);
    if (mode3d) {
        const arma::mat rh = {
                {1, 0,  0, 0},
                {0, 0,  1, 0},
                {0, -1, 0, 0},
                {0, 0,  0, 1},
        };
        glLoadMatrixd(rh.memptr());
    } else {
        const arma::mat eye = arma::eye(4,4);
        glLoadMatrixd(eye.memptr());
    }
}

void RenderWindow::Perspective(double aspectRatio, double nearClip, double farClip) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    double top = nearClip * std::tan(fov / 2 * toRad);
    double right = aspectRatio * top;
    if (mode3d) {
        glFrustum(-right, right, -top, top, nearClip, farClip);
    } else {
        glOrtho(-right, right, -top, top, nearClip, farClip);
    }
}

RenderWindow::RenderWindow(const Material& splash) {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

    window = glfwCreateWindow(PROJECT_WINDOW_DEFAULT_WIDTH, PROJECT_WINDOW_DEFAULT_HEIGHT, PROJECT_WINDOW_TITLE, NULL, NULL);
    if (window == NULL)
        throw runtime_error("Failed to create GLFW window");
    glfwMakeContextCurrent(window);
#ifdef __glad_h_
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
        throw runtime_error("Failed to initialize GLAD");
#endif
    GLFW_CB::bind(window, this);

    printf("OpenGL %s supported by this platform (required is OpenGL 3.3): \n", glGetString(GL_VERSION));

    //enable features for splash
    glEnable(GL_TEXTURE_2D);

    //prepare perspective
    gl_framebuffer_size_callback(PROJECT_WINDOW_DEFAULT_WIDTH, PROJECT_WINDOW_DEFAULT_HEIGHT);
    // !! From here on we use the default right hand coordinates !!
    //               !! x = right, y = back, z = up !!

    ///*//render splash; will be displayed until next glfwSwapBuffers in tick(ms)
    glClearColor (1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    splash.bind();
    glPushMatrix();
    glLoadIdentity();
    glBegin(GL_QUADS);
    glTexCoord2f(0,0); glVertex3f(-1,  1, -15e-1);
    glTexCoord2f(0,1); glVertex3f(-1, -1, -15e-1);
    glTexCoord2f(1,1); glVertex3f( 1, -1, -15e-1);
    glTexCoord2f(1,0); glVertex3f( 1,  1, -15e-1); //near plane offset is guessed
    glEnd();
    glPopMatrix();
    glfwSwapBuffers(window);
    glClearColor(0.2f,0.2f,0.3f,1.0f);
    ///*/

    //enable features
    glEnable( GL_DEPTH_TEST );
    glEnable( GL_ALPHA );
    glEnable( GL_NORMALIZE );
    glEnable( GL_CULL_FACE );
    glEnable( GL_LINE_SMOOTH );
    glCullFace( GL_BACK );
    glFrontFace( GL_CCW );
    glEnable( GL_LIGHTING );
    glEnable( GL_LIGHT0 );
    glLightfv( GL_LIGHT0, GL_POSITION, l0param_pos );
    glLightfv( GL_LIGHT0, GL_DIFFUSE, l0param_diffuse );
    glLightfv( GL_LIGHT0, GL_AMBIENT, l0param_ambient );
    glLightfv( GL_LIGHT0, GL_SPECULAR, l0param_specular );
    glLightModelf( GL_LIGHT_MODEL_LOCAL_VIEWER, 0.0 );

    glEnable( GL_COLOR_MATERIAL );
    glColorMaterial( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE );

    glEnable( GL_FRAMEBUFFER_SRGB );
    glEnable( GL_BLEND );
    glBlendEquationSeparate( GL_FUNC_ADD, GL_FUNC_ADD ); //No, that's not a typo. Read the wiki article.
    glBlendFuncSeparate( GL_CONSTANT_COLOR, GL_ZERO, GL_CONSTANT_COLOR, GL_ZERO );
    glBlendColor( 1.0f, 1.0f, 1.0f, 1.0f );

    Material::reset();
}

RenderWindow::~RenderWindow() {
    terminationRequested=true; //should already be true
    glfwSetWindowShouldClose(window, true);
    GLFW_CB::release(window);
}

void RenderWindow::translateCamera(double xaxis, double yaxis, double zaxis) {
    arma::vec dir = arma::normalise(camAt-camPos);
    arma::vec up = {0,0,1};
    arma::vec right = arma::normalise(arma::cross(dir, up));
    up = arma::normalise(arma::cross(right, dir));

    camPos += right * (xaxis);
    camPos += dir * (yaxis);
    camPos += up * (zaxis);
    camAt += right * (xaxis);
    camAt += dir * (yaxis);
    camAt += up * (zaxis);
}

void RenderWindow::tick(double dt) {
    processInput();
    glClearColor(0.2f,0.2f,0.3f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();
    LoadIdentity();
    camera_lookAt(camPos, camAt);
    glLightfv(GL_LIGHT0, GL_POSITION, l0param_pos);

    if (renderFun != nullptr) {
        renderFun(this,dt);
    }

    glPopMatrix();

    glfwSwapBuffers(window);
    glfwPollEvents();
}
