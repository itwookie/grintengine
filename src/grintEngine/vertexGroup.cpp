#include "vertexGroup.h"

void VertexGroup::draw() {
    glPushMatrix();
    // TODO check if those transformations are correct
    glTranslated(offset[0], offset[1], offset[2]);
    glRotated(rotation[0], 0.0, 0.0, 1.0);
    glRotated(rotation[1], 1.0, 0.0, 0.0);
    glRotated(rotation[2], 0.0, 1.0, 0.0);

    ssize_t mat = -1; uint_fast8_t i;
    for (Polygon polygon : polygons) {
        if (mat != polygon.m) {
            if (mat != -1) glEnd();
            MaterialAtlas.getMaterial(mat = polygon.m).bind();
            glBegin(GL_TRIANGLES);
        }
        for (i=0; i<3; i++) {
            glTexCoord2dv(vertices[polygon.p[i]].uvcoord.mem);
            glNormal3dv(vertices[polygon.p[i]].normal.mem);
            glVertex3dv(vertices[polygon.p[i]].position.mem);
        }
    }
    if (mat != -1) glEnd(); // it mat is still -1 no polygons were drawn

    glPopMatrix();
}