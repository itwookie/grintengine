cmake_minimum_required(VERSION 3.16)
project(2dgame)

set(CMAKE_CXX_STANDARD 17)

# Keep this to ONLY include the './include' directory
include_directories(include include/grintEngine)

add_compile_definitions(ARMA_DONT_USE_WRAPPER)

# It's mofokin 2020, let's please step away from ascii - thanks
# utf8 stuff stolen from some google cached gist that's gone
add_definitions(-DCHARSET_UTF_8 -DMECAB_CHARSET=utf-8 -DMECAB_UTF8_USE_ONLY)
foreach (flag CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO)
    set(${flag} "${${flag}} -finput-charset=UTF-8 -fexec-charset=UTF-8")
endforeach ()

add_executable(2dgame
        include/grintEngine/KHR/khrplatform.h
        include/grintEngine/glad/glad.h
        src/grintEngine/glad.c
        src/main.cpp
        include/grintEngine/opengl_base.h
        src/grintEngine/RenderWindow.cpp
        include/grintEngine/RenderWindow.h
        include/grintEngine/GlfwInstances.h
        src/grintEngine/GlfwInstances.cpp
        include/grintEngine/NonAssignable.h
        include/grintEngine/material.h
        src/grintEngine/material.cpp
        include/grintEngine/vertexGroup.h
        src/grintEngine/vertexGroup.cpp
        include/grintEngine/controller.h
        include/grintEngine/grintEngine.h)

TARGET_LINK_LIBRARIES(2dgame opengl32 glu32 glfw3 gdi32 png z openblas lapack gfortran quadmath)