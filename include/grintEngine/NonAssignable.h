#ifndef INC_2DGAME_NONASSIGNABLE_H
#define INC_2DGAME_NONASSIGNABLE_H

class NonAssignable {
public:
    NonAssignable(NonAssignable const&) = delete;
    NonAssignable& operator=(NonAssignable const&) = delete;
    NonAssignable() {}
};

#endif //INC_2DGAME_NONASSIGNABLE_H
