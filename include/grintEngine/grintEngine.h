#ifndef INC_2DGAME_GRINTENGINE_H
#define INC_2DGAME_GRINTENGINE_H

#include "opengl_base.h"
#include "controller.h"
#include "material.h"
#include "vertexGroup.h"
#include "RenderWindow.h"

void startGameLoop(RenderWindow* render) {
    double t0 = glfwGetTime(), dt=0.0;
    while (bool(*render)) {
        try {
            render->tick(dt);
        } catch (exception& e) {
            cerr << e.what() << endl;
            break;
        }
        dt = glfwGetTime()-t0;
        t0 = glfwGetTime();
    }
}

#endif //INC_2DGAME_GRINTENGINE_H
