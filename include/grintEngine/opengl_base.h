#ifndef INC_2DGAME_OPENGL_BASE_H
#define INC_2DGAME_OPENGL_BASE_H

#include <glad/glad.h>
//#include <GL/gl.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <armadillo>
#include <stdexcept>
#define _USE_MATH_DEFINES
#include <cmath>

#define toDeg (180.0/M_PI)
#define toRad (M_PI/180.0)

using namespace std;
using namespace arma;

#endif //INC_2DGAME_OPENGL_BASE_H
