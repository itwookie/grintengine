#ifndef INC_2DGAME_VERTEXGROUP_H
#define INC_2DGAME_VERTEXGROUP_H

#include "material.h"

#include <vector>
#include <armadillo>

using namespace std;
using namespace arma;

struct Vertex {
    vec3 position;
    vec3 normal={0,0,1}; //default as speced by khronos docs
    vec2 uvcoord={0,0};
};

struct Polygon {
    size_t p[3],m; //p are point indices into vertex vector, m is the material handle from the material atlas
};

struct VertexGroup {
    vector<Vertex> vertices;
    vector<Polygon> polygons;
    vec3 offset;
    vec3 rotation; //as pitch yaw roll
    void draw();
};

#endif //INC_2DGAME_VERTEXGROUP_H
