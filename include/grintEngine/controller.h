/*
 * A controller implements behaviour for mouse, keyboard and gamepad input.
 * If the controller can process input ( the key is bound ) it notifies the parent.
 */
#ifndef INC_2DGAME_CONTROLLER_H
#define INC_2DGAME_CONTROLLER_H

#include "opengl_base.h"

class RenderWindow;

class Controller {
private:
    bool transparent = false;
protected:
    /**
     * @param transparent this means that, in case the controller has no assignment for an action,
     * the next controller in the stack might be asked for input processing
     */
    Controller(bool transparent) : transparent(transparent) {}
public:
    virtual bool mouse_move(RenderWindow* render, double x, double y) = 0;
    virtual bool mouse_button(RenderWindow* render, int button, int action, int mods) = 0;
    virtual bool mouse_wheel(RenderWindow* render, double xoffset, double yoffset) = 0;
    virtual bool key_press(RenderWindow* render, int key, int scancode, int action, int mods) = 0;
    virtual bool key_poll(RenderWindow* render, GLFWwindow* glwindow) = 0;
    inline bool isTransparent() {
        return transparent;
    }
};

#endif //INC_2DGAME_CONTROLLER_H
