#ifndef INC_2DGAME_RENDERWINDOW_H
#define INC_2DGAME_RENDERWINDOW_H

#define PROJECT_WINDOW_CLASS_NAME "RobotRender"
#define PROJECT_WINDOW_TITLE "Robot Render"
#define PROJECT_WINDOW_DEFAULT_WIDTH 720
#define PROJECT_WINDOW_DEFAULT_HEIGHT 720

#define MOUSE_SENSITIVITY_X 4.0
#define MOUSE_SENSITIVITY_Y 3.0
#define MOUSE_SCROLLSPEED 1e-1
#define MOUSE_MOVESPEEDMULT 1.0f

#define CAM_INITIAL_POSITION { 0, -1, 0 }
#define CAM_INITIAL_TARGET { 0, 0, 0 }
//min vof is unused, zoom now translates
#define CAM_FOV_MAX 65.0
#define CAM_FOV_MIN 15.0

/* Prevent copying instance, as that might call constructors / destructors at unexpected times.
 * Making it non-assignable prevents the creation of multiple gl-instances.
 */
#include "NonAssignable.h"
#include "opengl_base.h"
#include "material.h"
#include "controller.h"
#include <memory>

typedef void (*SceneRender)(RenderWindow* render, double dt);

class RenderWindow : public NonAssignable {
    GLFWwindow* window;
    vec         camPos         = CAM_INITIAL_POSITION;
    vec         camAt          = CAM_INITIAL_TARGET;
    double      fov            = CAM_FOV_MAX;
    bool        mode3d         = true;
    double      aspectRatio    = 1.0;
    double      clipPlaneNear  = 1.0;
    double      clipPlaneFar   = 100.0;
    SceneRender renderFun      = nullptr;

    bool        terminationRequested = false;

    float l0param_pos[4]      = {-0.1,-0.2,1.0,0.0};
    float l0param_diffuse[4]  = {1.0,1.0,1.0,1.0};
    float l0param_ambient[4]  = {0.1,0.1,0.1,1.0};
    float l0param_specular[4] = {1.0,1.0,1.0,1.0};

    vector<unique_ptr<Controller>> keyBinds;

    /**
     * Convenience method that positions the camera at c and makes it point towards the coordiantes of t
     * @param c new camera position
     * @param t target point for camera
     */
    void camera_lookAt(arma::vec& c, arma::vec& t);
    /**
     * Rotate an arbitrary point around the given roation center using <yaw, pitch> rotations.
     * Writes the new coordinates back into point
     * @param center point to rotate around
     * @param point the point to modify
     * @param zaxis yaw rotation in radian
     * @param xaxis pitch rotation in radian
     */
    void rotatePoint(const arma::vec& center, arma::vec& point, const double zaxis, const double xaxis);

    /**
     * The polling alternative to gl_key_callback. Might be easier for continuous controls like movement
     */
    void processInput();

public:
    void gl_framebuffer_size_callback(int width, int height);
    void gl_cursor_position_callback(double xpos, double ypos);
    void gl_mouse_button_callback(int button, int action, int mods);
    void gl_scroll_callback(double xoffset, double yoffset);
    void gl_key_callback(int key, int scancode, int action, int mods);
    void LoadIdentity();
    void Perspective(double aspectRatio, double nearClip, double farClip);
    void switchMode(bool to3d);

    /** @param dt is time since last frame */
    void tick(double dt);
    /** @return true while the game is supposed to run */
    operator bool() const {
        return !terminationRequested;
    }
    inline void close() {
        terminationRequested=true;
    }

    inline GLFWwindow* getGLFWwindow() {
        return window;
    }

    /**
     * push new keybindings on top of the stack. for example vehicle controls over global menuing.
     * @param controller an implementation of Controller
     */
    template<class T>
    inline void pushKeybinds(const T& controller) {
        //make unique ptr copy to allow the controller within the vector of base type Controller
        keyBinds.push_back(make_unique<T>(controller));
    }
    inline void popKeybinds() {
        keyBinds.pop_back();
    }
    inline void clearKeybinds() {
        keyBinds.clear();
    }

    inline void setSceneRender(SceneRender render) {
        renderFun = render;
    }

    /**
     * Takes a one time user material to render a splash screen.
     * For ease of use the material is released within the constructor.
     */
    RenderWindow(const Material& splash);
    ~RenderWindow();

    /** floating windows are alyways on top */
    inline void setFloating(bool floating) {
        glfwSetWindowAttrib(window, GLFW_FLOATING, floating ? GLFW_TRUE : GLFW_FALSE);
    }
    inline void rotateObject(double zaxis, double xaxis) {
        rotatePoint(camAt, camPos, zaxis, xaxis);
    }
    inline void rotateCamera(double zaxis, double xaxis) {
        rotatePoint(camPos, camAt, zaxis, xaxis);
    }
    inline void translateCamera(double xaxis, double yaxis, double zaxis);
    inline void zoomCamera(double towards);
    inline void addFieldOfView(double amount) {
        fov += amount;
        if (fov < CAM_FOV_MIN) fov = CAM_FOV_MIN;
        else if (fov > CAM_FOV_MAX) fov = CAM_FOV_MAX;

        //update render
        GLint viewport[4];
        glGetIntegerv(GL_VIEWPORT, viewport);
        gl_framebuffer_size_callback(viewport[2], viewport[3]);
    }
    /** this switches the render into a mode, that lets you draw lines */
    inline void _startLineRender() {
        glDisable( GL_LIGHTING );
        glBlendFuncSeparate( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA ); //transparenz funktioniert
    }
    /** turns line mode back off - it's important to reconfigure EVERY light! */
    inline void _endLineRender() {
        glEnable( GL_LIGHTING );
        glEnable( GL_LIGHT0 );
        glLightfv(GL_LIGHT0, GL_AMBIENT, l0param_ambient);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, l0param_diffuse);
        glLightfv(GL_LIGHT0, GL_SPECULAR, l0param_specular);
        glLightfv(GL_LIGHT0, GL_POSITION, l0param_pos);
        glBlendFuncSeparate( GL_CONSTANT_COLOR, GL_ZERO, GL_CONSTANT_COLOR, GL_ZERO ); //texturen sind einfärbbar
    }
};


#endif //INC_2DGAME_RENDERWINDOW_H
