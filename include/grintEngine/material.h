#ifndef INC_2DGAME_MATERIAL_H
#define INC_2DGAME_MATERIAL_H

#include "opengl_base.h"
#include "NonAssignable.h"
#include <string>
#include <map>
#include <vector>

using namespace std;

class ImageTexture {
    GLuint hdlTexture; //opengl texture handle
    void constructRaw(const uint8_t *memory, size_t width, size_t height);
public:
    /**
     * Contruct ImageTexture from file
     */
    ImageTexture(const char* pngfilename);
    /**
     * Construct ImageTexture form RAW memory pointer.
     */
    ImageTexture(const uint8_t *memory, size_t offset, size_t width, size_t height);
    /**
     * Construct ImageTexture form ZIP-compressed memory pointer.
     */
    ImageTexture(const uint8_t *memory, size_t offset, size_t size, size_t decompSize, size_t width, size_t height);
    ~ImageTexture();
    void apply() const;
};
class TaintTexture {
    GLfloat color[4];
public:
    TaintTexture(float v):color{v,v,v,1.0f}{}
    TaintTexture(float r, float g, float b):color{r,g,b,1.0f}{}
    TaintTexture(float r, float g, float b, float a):color{r,g,b,a}{}
    inline void apply() const {
        glColor4fv(color);
    }
    inline void apply(GLenum GL_MATERIAL) const {
        glMaterialfv(GL_FRONT, GL_MATERIAL, color);
    }
};

class Material {
    TaintTexture ambientTaint;
    TaintTexture diffuseTaint;
    TaintTexture specularTaint;
    TaintTexture emissionTaint;
    float specularShininess;

    TaintTexture colorTaint;
    optional<ImageTexture> colorTexture;
public:
    Material();
    void bind() const;
    /** color4fv, default [1,1,1,1]
     * @return self for chaining */
    Material& setColor(const TaintTexture& taint);
    /** ambient, default [.2,.2,.2,1]
     * @return self for chaining */
    Material& setAmbient(const TaintTexture& taint);
    /** diffuse, default [.8,.8,.8,1]
     * @return self for chaining */
    Material& setDiffuse(const TaintTexture& taint);
    /** set both ambient and diffuse
     * @return self for chaining */
    Material& setAmbientDiffuse(const TaintTexture& taint);
    /** specular, default [0,0,0,1]
     * @return self for chaining */
    Material& setSpecular(const TaintTexture& taint);
    /** specular shininess exponent [0,128], default 0
     * @return self for chaining */
    Material& setShininess(float shininess);
    /** emission, default [0,0,0,1]
     * @return self for chaining */
    Material& setEmission(const TaintTexture& taint);
    /** set a image texture
     * @return self for chaining */
    Material& setMaterial(const ImageTexture& texture);
    static void reset();
};

class MaterialAtlas_t : public NonAssignable {
    vector<string> names;
    vector<Material> materials;
    vector<size_t> usecount;

    map<size_t, size_t> hdlIdx;
    vector<size_t> freeHdls; //handles that were freed and can be used again
    size_t maxHandle=0; //the first handle count that is unused after hdlIdx keys max
public:
    /** @return handle */
    size_t add(const string& name, Material& material);
    /** @return the handle for the material name or -1 if not registered */
    ssize_t getHandle(const string& name);
    Material& getMaterial(size_t handle);
    void remove(size_t handle);
};
extern MaterialAtlas_t MaterialAtlas;

#endif //INC_2DGAME_MATERIAL_H
