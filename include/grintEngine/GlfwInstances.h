/**
 * Purpose of this class is to manage the glfw callbacks that are static(window, args)
 * and redirect them to window->member(args)
 */
#ifndef INC_2DGAME_GLFWINSTANCES_H
#define INC_2DGAME_GLFWINSTANCES_H

#include "opengl_base.h"
#include "RenderWindow.h"
#include <memory>
#include <map>

void gl_global_callback_framebuffer_size(GLFWwindow* window, int witdth, int height);
void gl_global_callback_cursor_position(GLFWwindow* window, double xpos, double ypos);
void gl_global_callback_mouse_button(GLFWwindow* window, int button, int action, int mods);
void gl_global_callback_scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void gl_global_callback_keystroke(GLFWwindow* window, int key, int scancode, int action, int mods);
void gl_global_callback_close(GLFWwindow *window);
void gl_static_callback_windowmode(GLFWwindow *window, int maximized);
void gl_static_callback_error(int error, const char *description);

using namespace std;

class GLFW_CB {
private:
    //static in .h makes this a forward declaration. definition in .c required
    static map<GLFWwindow*, RenderWindow*> instanceMap;
public:
    static void bind(GLFWwindow* glwindow, RenderWindow* instance);
    static void release(GLFWwindow* glwindow);
    static RenderWindow* find(GLFWwindow* glwindow);
};

#endif //INC_2DGAME_GLFWINSTANCES_H
